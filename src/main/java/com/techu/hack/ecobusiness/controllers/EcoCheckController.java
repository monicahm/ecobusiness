package com.techu.hack.ecobusiness.controllers;

import com.techu.hack.ecobusiness.models.EcoCheckModel;
import com.techu.hack.ecobusiness.services.BusinessServiceResponse;
import com.techu.hack.ecobusiness.services.EcoCheckServiceResponse;
import com.techu.hack.ecobusiness.services.EcoCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ecobusiness/v1")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class EcoCheckController {

    @Autowired
    EcoCheckService ecoCheckService;

    @GetMapping("/checks")
    public ResponseEntity<List<EcoCheckModel>> listEcoChecks (){
        System.out.println("listEcoChecks");
        List <EcoCheckModel> result = this.ecoCheckService.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/checks")
    public ResponseEntity<EcoCheckServiceResponse> addEcoCheck (@RequestBody EcoCheckModel newEcoCheck){

        System.out.println("addEcoCheck");
        System.out.println("El id de nuevo EcoCheck es: " + newEcoCheck.getId());
        System.out.println("El nombre de nuevo EcoCheck es: " + newEcoCheck.getDescription());


        //Devolvemos un responseEntity con el nuevo check y un 201 de respuesta (Created)
        return new ResponseEntity<>(this.ecoCheckService.addEcoCheck(newEcoCheck), HttpStatus.CREATED);
    }

    @DeleteMapping("/checks/{id}")
    public ResponseEntity<BusinessServiceResponse> deleteEcoCheck(@PathVariable String id) {
        System.out.println("deleteEcoCheck");
        System.out.println("El id de EcoCheck a borrar es: " + id);

        ResponseEntity response = null;

        EcoCheckModel sEcoCheck = this.ecoCheckService.findById(id).getEcoCheckModel();

        if (sEcoCheck != null) {
            System.out.println("EcoCheck encontrado");
            EcoCheckServiceResponse result = this.ecoCheckService.delete(sEcoCheck);
            System.out.println("EcoCheck borrado");
            response = new ResponseEntity(result.getMsg(), result.getHttpStatus());
        } else {
            System.out.println("EcoCheck no encontrado");
            response = new ResponseEntity("EcoCheck no encontrado", HttpStatus.NOT_FOUND);
        }

        return response;
    }

    @GetMapping("/checks/{id}")
    public ResponseEntity<EcoCheckServiceResponse> getEcoCheck (@PathVariable String id) {
        System.out.println("getEcoCheck");
        System.out.println("Id de búsqueda es : " + id);

        EcoCheckServiceResponse response = this.ecoCheckService.findById(id);

        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
