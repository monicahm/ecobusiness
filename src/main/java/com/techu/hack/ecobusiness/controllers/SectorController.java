package com.techu.hack.ecobusiness.controllers;

import com.techu.hack.ecobusiness.models.SectorModel;
import com.techu.hack.ecobusiness.services.BusinessServiceResponse;
import com.techu.hack.ecobusiness.services.SectorServiceResponse;
import com.techu.hack.ecobusiness.services.SectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ecobusiness/v1")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class SectorController {

    @Autowired
    SectorService sectorService;

    @GetMapping("/sectors")
    public ResponseEntity<List<SectorModel>> listSectors (){
        System.out.println("listSectors");
        List <SectorModel> result = this.sectorService.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/sectors")
    public ResponseEntity<SectorServiceResponse> addSector (@RequestBody SectorModel newSector){

        System.out.println("addSector");
        System.out.println("El id de nuevo sector es: " + newSector.getId());
        System.out.println("El nombre de nuevo sector es: " + newSector.getDescription());


        //Devolvemos un responseEntity con el nuevo negocio y un 201 de respuesta (Created)
        return new ResponseEntity<>(this.sectorService.addSector(newSector), HttpStatus.CREATED);
    }

    @DeleteMapping("/sectors/{id}")
    public ResponseEntity<BusinessServiceResponse> deleteSector(@PathVariable String id) {
        System.out.println("deleteSector");
        System.out.println("El id de sector a borrar es: " + id);

        ResponseEntity response = null;

        SectorModel sSector = this.sectorService.findById(id).getSector();

        if (sSector != null) {
            System.out.println("Sector encontrado");
            SectorServiceResponse result = this.sectorService.delete(sSector);
            System.out.println("Sector borrado");
            response = new ResponseEntity(result.getMsg(), result.getHttpStatus());
        } else {
            System.out.println("Sector no encontrado");
            response = new ResponseEntity("Sector no encontrado", HttpStatus.NOT_FOUND);
        }

        return response;
    }

    @GetMapping("/sectors/{id}")
    public ResponseEntity<SectorServiceResponse> getSector (@PathVariable String id) {
        System.out.println("getSector");
        System.out.println("Id de búsqueda es : " + id);

        SectorServiceResponse response = this.sectorService.findById(id);

        return new ResponseEntity<>(response, response.getHttpStatus());
    }
}
