package com.techu.hack.ecobusiness.controllers;


import com.techu.hack.ecobusiness.models.BusinessModel;
import com.techu.hack.ecobusiness.services.BusinessService;
import com.techu.hack.ecobusiness.services.BusinessServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.time.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/ecobusiness/v1")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST,RequestMethod.DELETE,RequestMethod.PUT})
public class BusinessController {

    @Autowired
    BusinessService businessService;

    @GetMapping("/business")
    public ResponseEntity<List<BusinessModel>> listBusiness (@RequestParam(required = false, name = "rating") Integer rating){
        System.out.println("listBusiness");
        List <BusinessModel> result = this.businessService.findAll(rating);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/business/{id}")
    public ResponseEntity<BusinessServiceResponse> getBusiness (@PathVariable String id) {
        System.out.println("getBusiness");
        System.out.println("Id de búsqueda es : " + id);

        BusinessServiceResponse response = this.businessService.findById(id);

        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PostMapping("/business")
    public ResponseEntity<BusinessServiceResponse> addBusiness (@RequestBody BusinessModel newBusiness){

        System.out.println("addBusiness");

        System.out.println("El id de nueva empresa es: " + newBusiness.getId());
        System.out.println("El nombre de nueva empresa es: " + newBusiness.getName());
        System.out.println("Fecha: "+newBusiness.getRegistrationDate());

        BusinessModel sBusiness = new BusinessModel(null, newBusiness.getName(), newBusiness.getRegistrationDate(),newBusiness.getSector(), newBusiness.getAdditionalInfo(), newBusiness.getChecks());

        BusinessServiceResponse response = this.businessService.addBusiness(sBusiness);

        return new ResponseEntity<>(response, response.getHttpStatus());
    }

    @PutMapping("/business/{id}")
    public ResponseEntity<BusinessServiceResponse> updateBusiness (@RequestBody BusinessModel business, @PathVariable String id){

        System.out.println("updateBusiness");
        System.out.println("El id de empresa a actualizar: " + id);
        System.out.println("El nombre de nueva empresa es: " + business.getName());
        System.out.println("El id de nueva empresa es: " + business.getId());

        ResponseEntity response = null;

        //buscar por id
        BusinessModel sBusiness = this.businessService.findById(id).getBusiness();

        if (sBusiness != null){
            //actualizar producto
            sBusiness.setId(id);
            sBusiness.setName(business.getName());
            sBusiness.setAdditionalInfo(business.getAdditionalInfo());
            sBusiness.setRegistrationDate(business.getRegistrationDate());
            sBusiness.setChecks(business.getChecks());
            sBusiness.setSector(business.getSector());

            //añadimos para actualizar
            BusinessServiceResponse result = businessService.updateBusiness(sBusiness);
            System.out.println("Empresa actualizada");
            response = new ResponseEntity(result.getMsg(),result.getHttpStatus());
        }else {
            System.out.println("Empresa no encontrada");
            response = new ResponseEntity("Empresa no encontrada",HttpStatus.NOT_FOUND);
        }

        return response;
    }

    @DeleteMapping("/business/{id}")
    public ResponseEntity<BusinessServiceResponse> deleteBusiness(@PathVariable String id) {
        System.out.println("deleteBusiness");
        System.out.println("El id de empresa a borrar es: " + id);

        ResponseEntity response = null;

        BusinessModel sBusiness = this.businessService.findById(id).getBusiness();

        if (sBusiness != null) {
            System.out.println("Empresa encontrada");
            BusinessServiceResponse result = this.businessService.delete(sBusiness);
            System.out.println("Empresa borrada");
            response = new ResponseEntity(result.getMsg(), result.getHttpStatus());
        } else {
            System.out.println("Empresa no encontrada");
            response = new ResponseEntity("Empresa no encontrada", HttpStatus.NOT_FOUND);
        }

        return response;
    }

}
