package com.techu.hack.ecobusiness.controllers;

import com.techu.hack.ecobusiness.models.StarStatisticsModel;
import com.techu.hack.ecobusiness.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/ecobusiness/v1")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class StatisticsController {

    @Autowired
    StatisticsService statisticsService;

    @GetMapping("/statistics")
    public StarStatisticsModel getStatistics(){
        System.out.println("getStatistics");
        return this.statisticsService.getStatistics();
    }
}
