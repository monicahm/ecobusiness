package com.techu.hack.ecobusiness.models;

public class StarStatisticsModel {

    Long fiveStarsBusiness;
    Long fourStarsBusiness;
    Long threeStarsBusiness;
    Long twoStarsBusiness;
    Long oneStarBusiness;

    public StarStatisticsModel(){
        this.fiveStarsBusiness = 0L;
        this.fourStarsBusiness = 0L;
        this.threeStarsBusiness = 0L;
        this.twoStarsBusiness = 0L;
        this.oneStarBusiness = 0L;
    }

    public StarStatisticsModel(Long fiveStarsBusiness, Long fourStarsBusiness, Long threeStarsBusiness, Long twoStarsBusiness, Long oneStarBusiness){
        super();
        this.fiveStarsBusiness = fiveStarsBusiness;
        this.fourStarsBusiness = fourStarsBusiness;
        this.threeStarsBusiness = threeStarsBusiness;
        this.twoStarsBusiness = threeStarsBusiness;
        this.oneStarBusiness = oneStarBusiness;
    }

    public Long getFiveStarsBusiness() {
        return fiveStarsBusiness;
    }

    public void setFiveStarsBusiness(Long fiveStarsBusiness) {
        this.fiveStarsBusiness = fiveStarsBusiness;
    }

    public Long getFourStarsBusiness() {
        return fourStarsBusiness;
    }

    public void setFourStarsBusiness(Long fourStarsBusiness) {
        this.fourStarsBusiness = fourStarsBusiness;
    }

    public Long getThreeStarsBusiness() {
        return threeStarsBusiness;
    }

    public void setThreeStarsBusiness(Long threeStarsBusiness) {
        this.threeStarsBusiness = threeStarsBusiness;
    }

    public Long getTwoStarsBusiness() {
        return twoStarsBusiness;
    }

    public void setTwoStarsBusiness(Long twoStarsBusiness) {
        this.twoStarsBusiness = twoStarsBusiness;
    }

    public Long getOneStarBusiness() {
        return oneStarBusiness;
    }

    public void setOneStarBusiness(Long oneStarBusiness) {
        this.oneStarBusiness = oneStarBusiness;
    }
}
