package com.techu.hack.ecobusiness.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.util.List;
import java.time.*;

@Document(collection= "business")
public class BusinessModel {

    @Id
    String id;
    String name;
    LocalDate registrationDate;
    SectorModel sector;
    String additionalInfo;
    List<EcoCheckModel> checks;
    Integer rating;

    public BusinessModel() {

    }

    public BusinessModel(String id, String name, LocalDate registrationDate, SectorModel sector, String additionalInfo, List<EcoCheckModel> checks) {
        this.id = id;
        this.name = name;
        this.registrationDate = registrationDate;
        this.sector = sector;
        this.additionalInfo = additionalInfo;
        this.checks = checks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public SectorModel getSector() {
        return sector;
    }

    public void setSector(SectorModel sector) {
        this.sector = sector;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public List<EcoCheckModel> getChecks() {
        return checks;
    }

    public void setChecks(List<EcoCheckModel> checks) {
        this.checks = checks;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
