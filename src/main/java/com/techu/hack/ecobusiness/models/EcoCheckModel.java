package com.techu.hack.ecobusiness.models;

public class EcoCheckModel {

    String id;
    String description;
    boolean active;

    public EcoCheckModel(){
    }

    public EcoCheckModel(String id, String description, boolean active){
        this.id = id;
        this.description = description;
        this.active = active;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
