package com.techu.hack.ecobusiness.services;

import com.techu.hack.ecobusiness.models.BusinessModel;
import com.techu.hack.ecobusiness.models.EcoCheckModel;
import com.techu.hack.ecobusiness.models.StarStatisticsModel;
import com.techu.hack.ecobusiness.repositories.BusinessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatisticsService {
    @Autowired
    BusinessService businessService;

    public StarStatisticsModel getStatistics(){
        System.out.println("getStatistics");

        StarStatisticsModel statistics = new StarStatisticsModel();
        statistics.setFiveStarsBusiness(this.businessService.count(5));
        statistics.setFourStarsBusiness(this.businessService.count(4));
        statistics.setThreeStarsBusiness(this.businessService.count(3));
        statistics.setTwoStarsBusiness(this.businessService.count(2));
        statistics.setOneStarBusiness(this.businessService.count(1));
        return statistics;
    }


}
