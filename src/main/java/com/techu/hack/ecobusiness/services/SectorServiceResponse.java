package com.techu.hack.ecobusiness.services;


import com.techu.hack.ecobusiness.models.SectorModel;
import org.springframework.http.HttpStatus;

import java.awt.*;
import java.security.Provider;

public class SectorServiceResponse extends ServiceResponse {
    private SectorModel sector;

    public SectorServiceResponse () {
        super();
    }
    public SectorServiceResponse(SectorModel sectorModel) {
        super();
        this.sector = sectorModel;
    }

    public SectorModel getSector() {
        return sector;
    }

    public void setSectorModel(SectorModel sectorModel) {
        this.sector = sectorModel;
    }

}
