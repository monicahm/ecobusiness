package com.techu.hack.ecobusiness.services;

import com.techu.hack.ecobusiness.models.BusinessModel;
import org.springframework.http.HttpStatus;

import java.security.Provider;

public class BusinessServiceResponse extends ServiceResponse {

    private BusinessModel business;

    public BusinessServiceResponse () {
        super();
    }
    public BusinessServiceResponse(BusinessModel businessModel) {
        super();
        this.business = businessModel;
    }

    public BusinessModel getBusiness() {
        return business;
    }

    public void setBusinessModel(BusinessModel businessModel) {
        this.business = businessModel;
    }
}
