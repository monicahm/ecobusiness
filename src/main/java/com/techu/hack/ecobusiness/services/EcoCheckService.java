package com.techu.hack.ecobusiness.services;

import com.techu.hack.ecobusiness.models.EcoCheckModel;
import com.techu.hack.ecobusiness.repositories.EcoCheckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EcoCheckService {

    @Autowired
    EcoCheckRepository checkRepository;

    public List<EcoCheckModel> findAll() {
        System.out.println("findAll");
        return this.checkRepository.findAll();
    }

    public EcoCheckServiceResponse addEcoCheck(EcoCheckModel newCheck){
        System.out.println("addCheck");
        EcoCheckServiceResponse result = new EcoCheckServiceResponse();

        checkRepository.save(newCheck);

        result.setMsg("Check añadido correctamente");
        result.setHttpStatus(HttpStatus.CREATED);
        result.setEcoCheckModel(newCheck);

        return result;
    }

    public EcoCheckServiceResponse delete(EcoCheckModel check) {
        System.out.println("delete");

        EcoCheckServiceResponse result = new EcoCheckServiceResponse();
        this.checkRepository.delete(check);

        result.setMsg("Check borrado.");
        result.setHttpStatus(HttpStatus.OK);


        return result;
    }

    public EcoCheckServiceResponse findById (String id){
        System.out.println("findById");
        Optional<EcoCheckModel> result = this.checkRepository.findById(id);
        EcoCheckServiceResponse response = new EcoCheckServiceResponse();

        if (result.isPresent()) {
            response.setMsg("Check encontrado.");
            response.setHttpStatus(HttpStatus.OK);
            response.setEcoCheckModel(result.get());
        } else {
            response.setMsg("Check no encontrado.");
            response.setHttpStatus(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
