package com.techu.hack.ecobusiness.services;

import com.techu.hack.ecobusiness.models.BusinessModel;
import com.techu.hack.ecobusiness.models.SectorModel;
import com.techu.hack.ecobusiness.repositories.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SectorService {

    @Autowired
    SectorRepository sectorRepository;

    public List<SectorModel> findAll() {
        System.out.println("findAll");
        return this.sectorRepository.findAll();
    }

    public SectorServiceResponse addSector(SectorModel newSector){
        System.out.println("addSector");
        SectorServiceResponse result = new SectorServiceResponse();

        sectorRepository.save(newSector);

        result.setMsg("Sector añadido correctamente");
        result.setHttpStatus(HttpStatus.CREATED);
        result.setSectorModel(newSector);

        return result;
    }

    public SectorServiceResponse delete(SectorModel sector) {
        System.out.println("delete");

        SectorServiceResponse result = new SectorServiceResponse();
        this.sectorRepository.delete(sector);

        result.setMsg("Empresa borrada.");
        result.setHttpStatus(HttpStatus.OK);


        return result;
    }

    public SectorServiceResponse findById (String id){
        System.out.println("findById");
        Optional<SectorModel> result = this.sectorRepository.findById(id);
        SectorServiceResponse response = new SectorServiceResponse();

        if (result.isPresent()) {
            response.setMsg("Sector encontrado.");
            response.setHttpStatus(HttpStatus.OK);
            response.setSectorModel(result.get());
        } else {
            response.setMsg("Sector no encontrado.");
            response.setHttpStatus(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
