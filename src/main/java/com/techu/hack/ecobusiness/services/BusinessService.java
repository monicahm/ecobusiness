package com.techu.hack.ecobusiness.services;

import com.techu.hack.ecobusiness.models.BusinessModel;
import com.techu.hack.ecobusiness.models.EcoCheckModel;
import com.techu.hack.ecobusiness.repositories.BusinessRepository;
import com.techu.hack.ecobusiness.repositories.EcoCheckRepository;
import com.techu.hack.ecobusiness.repositories.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BusinessService {

    @Autowired
    BusinessRepository businessRepository;

    @Autowired
    SectorRepository sectorRepository;

    @Autowired
    EcoCheckRepository ecoCheckRepository;

    public List<BusinessModel> findAll(Integer rating) {
        System.out.println("findAll");
        if (rating!=null && rating<=5 && rating>=0){
            System.out.println("Filtrando por rating: " + rating);
            BusinessModel filterBusiness = new BusinessModel();
            filterBusiness.setRating(rating);
            return this.businessRepository.findAll(Example.of(filterBusiness));
        } else {
            System.out.println("Sin filtro");
            return this.businessRepository.findAll();
        }
    }

    public long count(Integer rating){
        System.out.println("count");
        if (rating!=null && rating<=5 && rating>=0){
            System.out.println("count con filtro");
            BusinessModel filterBusiness = new BusinessModel();
            filterBusiness.setRating(rating);
            return this.businessRepository.count(Example.of(filterBusiness));
        } else {
            System.out.println("count sin filtro");
            return this.businessRepository.count();
        }

    }

    public BusinessServiceResponse addBusiness(BusinessModel newBusiness){
        System.out.println("addBusiness");
        BusinessServiceResponse result = new BusinessServiceResponse();

        if (this.sectorRepository.findById(newBusiness.getSector().getId()).isEmpty() == true) {
            System.out.println("El sector no se ha encontrado");
            result.setMsg("El sector no existe");
            result.setHttpStatus(HttpStatus.BAD_REQUEST);
            return result;
        }

        for (EcoCheckModel check : newBusiness.getChecks()){
            if (this.ecoCheckRepository.findById(check.getId()).isEmpty()==true){
                System.out.println("Check no encontrado");
                result.setMsg("Algúno de los checks no existe");
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                return result;
            }
        }

        //calcular el rating para guardar la  empresa
        Integer rating = this.calculateRating(newBusiness);
        newBusiness.setRating(rating);
        businessRepository.insert(newBusiness);

        result.setMsg("Empresa añadida correctamente");
        result.setHttpStatus(HttpStatus.CREATED);
        result.setBusinessModel(newBusiness);

        return result;
    }

    public BusinessServiceResponse updateBusiness (BusinessModel business){
        System.out.println("updateBusiness");
        BusinessServiceResponse result = new BusinessServiceResponse();

        //calcular el rating para guardar la  empresa
        Integer rating = this.calculateRating(business);
        business.setRating(rating);

        businessRepository.save(business);

        result.setMsg("Empresa actualizada correctamente");
        result.setHttpStatus(HttpStatus.OK);
        result.setBusinessModel(business);

        return result;

    }

    public BusinessServiceResponse findById (String id){
        System.out.println("findById");
        Optional<BusinessModel> result = this.businessRepository.findById(id);
        BusinessServiceResponse response = new BusinessServiceResponse();

        if (result.isPresent()) {
            response.setMsg("Empresa encontrada.");
            response.setHttpStatus(HttpStatus.OK);
            response.setBusinessModel(result.get());
        } else {
            response.setMsg("Empresa no encontrada.");
            response.setHttpStatus(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    public BusinessServiceResponse delete(BusinessModel business) {
        System.out.println("delete");

        BusinessServiceResponse response = new BusinessServiceResponse();
        this.businessRepository.delete(business);

        response.setMsg("Empresa borrada.");
        response.setHttpStatus(HttpStatus.OK);

        return response;
    }

    private Integer calculateRating(BusinessModel businessModel){
        Integer checkCount = 0;
        Integer rating = 0;
        //recorrer los cheks y calcular
        for (EcoCheckModel check : businessModel.getChecks()){
            if (check.isActive()) {
                checkCount++;
            }
        }
        if (checkCount<2){
            rating = 1;
        }else if (checkCount>4){
            rating = 5;
        }else {
            rating = checkCount;
        }
        return rating;
    }

}