package com.techu.hack.ecobusiness.services;

import com.techu.hack.ecobusiness.models.EcoCheckModel;
import com.techu.hack.ecobusiness.models.SectorModel;

public class EcoCheckServiceResponse extends ServiceResponse {
    private EcoCheckModel ecoCheckModel;

    public EcoCheckServiceResponse () {
        super();
    }
    public EcoCheckServiceResponse(EcoCheckModel ecoCheckModel) {
        super();
        this.ecoCheckModel = ecoCheckModel;
    }

    public EcoCheckModel getEcoCheckModel() {
        return ecoCheckModel;
    }

    public void setEcoCheckModel(EcoCheckModel ecoCheckModel) {
        this.ecoCheckModel = ecoCheckModel;
    }

}
