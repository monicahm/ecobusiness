package com.techu.hack.ecobusiness.repositories;

import com.techu.hack.ecobusiness.models.EcoCheckModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

public interface EcoCheckRepository extends MongoRepository<EcoCheckModel, String> {
}
