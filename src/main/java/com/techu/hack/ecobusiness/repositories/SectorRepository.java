package com.techu.hack.ecobusiness.repositories;

import com.techu.hack.ecobusiness.models.BusinessModel;
import com.techu.hack.ecobusiness.models.SectorModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SectorRepository  extends MongoRepository<SectorModel, String> {
}
